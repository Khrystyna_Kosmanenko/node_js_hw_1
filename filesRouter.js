const express = require('express');
const router = express.Router()

const {createFile, getFileByName, updateFile, getFiles} = require('./filesServise')
const {checkExtension} = require('./checkExtension')

router.route('/').get(getFiles).post(checkExtension, createFile)
router.route('/:filename').get(getFileByName).put(updateFile)

module.exports = router;
