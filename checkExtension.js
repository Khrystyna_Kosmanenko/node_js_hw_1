const path = require('path')

const checkExtension = (req, res, next) => {
  const { filename } = req.body;
  if (!filename) {
    return res.status(400).json({message: `Please specify ' filename' parameter`});
  }
  
  if (!/^.(log|txt|json|yaml|xml|js)*$/gi.test(path.extname(filename))){
    return res.status(404).json({message: "Incorrect file extension"})
  }
  next()
}

module.exports = {checkExtension}