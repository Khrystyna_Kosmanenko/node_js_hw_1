const path = require('path')
const fs = require('fs')

const getFiles = (req, res) => {
  fs.readdir(path.join(__dirname, 'files'), (err, data) => {

    if(err) {
      return res.status(500).json({message: 'Server error'})
    }
   
    res.json({data: data}) 
  })
}

const createFile = (req, res) => {
  const { filename, content } = req.body;

  if (fs.readdirSync('./files').includes(filename)) {
    return res.status(400).json({message: `${filename} already exist`});
  }

  if (!content) {
    return res.status(400).json({message: `Please specify 'content' parameter`});
  }
 
  fs.writeFile(path.join(__dirname, 'files', filename), content, err => {
    if(err) {
      return res.status(500).json({message: 'Server error'})
    }
    return res.status(200).json({message: 'File created successfully'})
  })
};

const updateFile = (req, res) => {
  const { content } = req.body;

  const fileName = req.params.filename;

  fs.readFile(path.join(__dirname, 'files', fileName), (err) => {

    if(err) {
      return res.status(500).json({message: 'Server error'})
    }
    if (!content) {
      return res.status(400).json({message: `Please specify 'content' parameter`});
    }

    fs.writeFile(path.join(__dirname, 'files', fileName), content, (err,) => {
      if(err) {
        return res.status(500).json({message: 'Server error'})
      }
      
      res.json({message: `The data was update`})
    }) 
  }) 
};

const getFileByName = (req, res) => {
 const fileName = req.params.filename
 const extension = path.extname(fileName).slice(1)
 
  fs.readFile(path.join(__dirname, 'files', fileName), (err, data) => {

    if(err) {
      return res.status(400).json({message: `No file with ${fileName} filename found`})
    }

    fs.stat((path.join(__dirname, 'files', fileName)), (err, stat) => {

      if(err) {
        return res.status(500).json({message: 'Server error'})
      }

      res.status(200).json({
        "message": "Success",
        "filename": fileName,
        "content": data.toString(),
        "extension": extension,
        "uploadedDate": stat.birthtime
      }) 
    })
  })
}

module.exports = { getFileByName, updateFile, createFile, getFiles}